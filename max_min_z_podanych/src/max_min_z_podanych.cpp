#include <iostream>
using namespace std;

int main() {
	cout << "Program wypisujacy najwieksza oraz najmniejsza z podanych liczb."
			<< endl;
	int liczba;
	int min;
	int max;
	int opcja;
	while (opcja != 3) {
		cout << "Menu. Wybierz opcje: " << endl;
		cout << "1) Wprowadz liczby" << endl;
		cout << "2) Wypisz wartosc minimalna i maksymalna" << endl;
		cout << "3) Koniec" << endl;
		cin >> opcja;
		switch (opcja) {
		case 1: {
			cout << "Ile liczb chcesz wprowadzic? ";
			int j;
			cin >> j;
			cout << "Wprowadz liczby, kazda zatwierdzajaz klawiszem enter: ";
			cin >> liczba;
			min = liczba;
			max = liczba;
			for (int i=0; i<(j-1);++i){
			cin >> liczba;
			if (liczba < min)
				min = liczba;
			if (liczba > max)
				max = liczba;
			}
			break;
		}
		case 2: {
			cout << "Min = " << min << ", Max = " << max << endl;
			break;
		}
		case 3: {
			cout << "Wylaczam...";
			break;
		}
		default:{
			cout << "Niewlasciwa opcja. Sprobuj ponownie." << endl;
		}
		}
	}

	return 0;
}
