//============================================================================
// Name        : pole_powierzchnia_figury_przestrzenne.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <cmath>
using namespace std;

int main() {
	cout
			<< "Witaj w programie obliczajacym obj�tosc i pole powierzchni figur przestrzennych."
			<< endl;
	unsigned short int opcja(0);
	unsigned short int figura(0);
	while (figura != 3 && opcja != 5) { //w przypadku wyboru opcji wylacz w ktorymkolwiek z menu petla jest przerywana, program konczy sie
		figura = 0; // powoduje wyswietlenie 1 menu po kazdym przejsciu petli
		while (figura != 1 && figura != 2 && figura != 3) { // powoduje ponowne wyswietlenie menu w przypadku wybrania zlej opcji
			cout << "Objetosc i pole jakiej figury chcesz obliczyc?" << endl;
			cout << "1) Graniastoslup" << endl;
			cout << "2) Ostroslup" << endl;
			cout << "3) Wylacz program" << endl;
			cin >> figura;
			if (figura > 3 || figura == 0)
				cout << "Niewlasciwy wybor opcji. Sprobuj ponownie. " << endl;
		}
		if (figura == 1 || figura == 2) {
			cout << "Wybierz opcje: " << endl;
			if (figura == 1)
				cout << "Graniastoslup ";
			else
				cout << "Ostroslup ";
			cout << "o podstawie: " << endl;
			cout << "1) Kwadratu" << endl;
			cout << "2) Prostokata" << endl;
			cout << "3) Kola" << endl;
			cout << "4) Figura przestrzenna: Kula" << endl;
			cout << "5) Wylacz program" << endl;
			cin >> opcja;
			double x(0); //zmienna wykorzystywana we wzorach w zaleznosci czy liczymy objetosc graniastoslupa czy ostroslupa
			if (figura == 2)
				x = (1 / 3.0);
			else
				x = 1;
			const float pi(3.14);
			switch (opcja) {
			case 1: {
				cout << "Wprowadz dane: " << endl;
				cout << "Dlugosc boku podstawy: ";
				int a;
				cin >> a;
				cout << "Wysokosc figury: ";
				int h;
				cin >> h;
				double obj;
				obj = x * a * a * h;
				cout << "Objetosc V = " << obj << endl;
				double pole;
				if (figura == 1) // powierzchnia graniastoslupa
					pole = 2 * a * a + 4 * a * h;
				else { // powierzchnia ostroslupa
					double h1(0);
					h1 = sqrt(pow(h, 2) + pow(a / 2, 2));
					pole = pow(a, 2) + 4 * 0.5 * a * h1;
				}
				cout << "Pole P = " << pole << endl;
				break;
			}
			case 2: {
				cout << "Wprowadz dane: " << endl;
				cout << "Dlugosc boku podstawy: ";
				int a;
				cin >> a;
				cout << "Szerokosc boku podstawy: ";
				int b;
				cin >> b;
				cout << "Wysokosc figury: ";
				int h;
				cin >> h;
				double obj;
				obj = x * a * b * h;
				cout << "Objetosc V = " << obj << endl;
				double pole;
				if (figura == 1)
					pole = 2 * a * b + 2 * a * h + 2 * b * h; // powierzchnia graniastoslupa
				else { // powierzchnia ostroslupa
					double h1(0);
					double h2(0);
					h1 = sqrt(pow(h, 2) + pow(a / 2, 2));
					h2 = sqrt(pow(h, 2) + pow(b / 2, 2));
					pole = pow(a, 2) + a * h1 + b * h2;
				}
				cout << "Pole P = " << pole << endl;
				break;
			}
			case 3: {
				cout << "Wprowadz dane: " << endl;
				cout << "Dlugosc promienia podstawy: ";
				int r;
				cin >> r;
				cout << "Wysokosc figury: ";
				int h;
				cin >> h;
				double obj;
				obj = x * pi * pow(r, 2) * h;
				cout << "Objetosc V = " << obj << endl;
				double pole;
				if (figura == 1) // powierzchnia graniastoslupa
					pole = 2 * pi * pow(r, 2) + 2 * pi * r * h;
				else { // powierzchnia ostroslupa
					double h1(0);
					h1 = sqrt(pow(h, 2) + pow(r, 2));
					pole = pi * pow(r, 2) + pi * r * h1;
				}
				cout << "Pole P = " << pole << endl;

				break;
			}
			case 4: {
				cout << "Wprowadz dane: " << endl;
				cout << "Dlugosc promienia kuli: ";
				int r;
				cin >> r;
				double obj;
				obj = 4 / 3.0 * pi * pow(r, 3);
				cout << "Objetosc V = " << obj << endl;
				double pole;
				pole = 4 * pi * pow(r, 2);
				cout << "Pole P = " << pole << endl;
				break;
			}
			case 5: { // w przypadku wyboru opcji wylacz w 2 menu
				cout << "Wylaczam..." << endl;
				break;
			}
			default:
				cout << "Niewlasciwy wybor opcji. Sprobuj ponownie" << endl;
			}
		} else
			// w przypadku wyboru opcji wylacz w 1 menu
			cout << "Wylaczam..." << endl;
	}
	return 0;
}
