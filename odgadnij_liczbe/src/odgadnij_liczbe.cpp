//============================================================================
// Name        : odgadnij_liczbe.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

int main() {
	srand(time(NULL)); // ustawia losowy punkt startowy generatora pseudolosowych liczb
	cout << "Program losuje liczbe od 1 do 20. Sprobuj ja odgadnac: " << endl;
	int los;
	los = (rand() % 20) + 1; // zakres losowych liczb od 1 do 20
	int i(0);
	int liczba(0);
	while (liczba != los) { // przerywa petle gdy uzytkownik odgadnie wylosowana liczbe
		cout << "Podaj liczbe: ";
		cin >> liczba;
		++i;
		if (liczba != los) {
			if (liczba > los)
				cout << "Wprowdziles liczbe wieksza od wylosowanej!" << endl;
			else
				cout << "Wprowadziles liczbe mniejsza od wylosowanej!" << endl;
			cout << "Sprobuj ponownie. " << endl;
		}
	}
	cout << "Gratulacje, zgadles!" << endl;
	cout << "Wylosowana liczba to: " << los << " Potrzebowales " << i
			<< " prob." << endl;
	return 0;
}
