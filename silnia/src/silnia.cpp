//============================================================================
// Name        : silnia.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main() {
	cout << "Program obliczajacy silnie dla podanej liczby (zakres <0,12>)"
			<< endl;
	// zakres <0,12> bo silnia jest zdefiniowana tylko dla dodatniej i silnia >12 nie miesci sie w int
	unsigned short liczba;
	unsigned int wynik(1);
	while (liczba > 12) {
		cout << "Wprowadz liczbe: ";
		cin >> liczba;

		if (liczba > 12)
			cout << "Libcza nie nalezy do zakresu <0,12>. Wprowadz ponownie."
					<< endl;
		else if (liczba > 0) {
			for (int i = 1; i <= liczba; ++i) {
				wynik *= i;
			}
			cout << liczba << "! = " << wynik;
		} else
			cout << "0! = 1";
	}
	return 0;
}
