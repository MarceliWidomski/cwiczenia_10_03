//============================================================================
// Name        : ciag_fibonacciego.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main() {
	unsigned int temp;
	unsigned int liczba1(0);
	unsigned int liczba2(1);
	unsigned int ilosc;

	cout << "Program zwracajacy ciag Fibonacciego" << endl;
	cout << "Podaj ilosc liczb ciagu: ";
	cin >> ilosc;
	cout << liczba1 << " ";
	for (int i = 2; i <= ilosc; i++) {
		temp = liczba1 + liczba2;
		if (temp < liczba2) { // przerywa petle i wyswietla komunikat gdy wartosc temp przekroczy dopuszczalna wartosc unsigned int
			cout << "Liczba przekroczyla dostepny zakres" << endl;
			break;
		}
		liczba1 = liczba2;
		liczba2 = temp;
		cout << temp << " ";
	}
	return 0;
}
